# cciConnect

**CCI CONNECTIONS**


**REQUIREMENTS AND DESIGN DOCUMENTATION**


PROJECT OVERVIEW

•	This project is mainly interested in connecting people who wish to organize certain events based on certain categories.
•	Users who want to organize an event under certain categories define what the event is and what the category is.
•	When another user logs in, there is a complete list of connections available which show all the available events based on dates, time and location for convenience. The user can then login and RSVP for an interested event.
•	The project is based on MVC architecture where the controller is the heart of the application. For persistent database storage we have used MongoDB(NoSQL) and mongoose for ODM. Every part of this application is object oriented and the operations happen on this data.
•	For simplicity the whole application is divided into views, models and controllers.
o	View: User interface is the defining factor for view. All these views are referred to as “.ejs” files, that are constructed and structured using HTML5 language and CSS3 for styling attributes. We also make use of Bootstrap4 architecture and Font Awesome for playing with fonts on the user interface. 
o	Model: The model can be viewed as a module as to how the data to be stored is structured before any database operation takes place. We define statics or functions in our models to access or write to the database. We can view these functions as properties of an object for simplicity
o	Controller: The heart of the application is the controller module. This controller acts as an intermediate logic that binds and renders views with databases. The controller is mainly written following Node.JS architecture using JavaScript. Controller is responsible for correct routing, encryption/decryption, database module creation and finally the run the application.
•	The target market for this application are communities who are closely knit and want to organize events. But the application is diverse enough that people who do are not a part of the community can also join and expand their connections.




**PAGE DESIGN**
 
Sign In Page:
 
The purpose of the page is to login a user securely to access the website and all of its features provided the right credentials are given.

Audience:
Audience of the page are people who have signed up to the website or willing to sign up to the website

There are 2 fields on this page:
	Username: takes a valid and identified password
	Password: A minimum of 6-character field to login the user after validation

Validation:
The users email and password are validated as follows:
Data provided in email field to access the registered user record an encrypted password which matches the decrypted data in the database.

Login Button:
	This button will validate the user:
		Successful login: Redirect to homepage
		Unsuccessful login: Redirect to login page with a failure to login message
 

Sign Up Page:
 
The purpose of the page is to register a user securely to access the website and all of its features provided the right credentials are given.

Audience:
People who have signed up to the website or willing to sign up to the website.

There are 4 fields on this page:
	First Name: Users First Name
	Last Name: Users Last Name
	Username: Takes a valid and identified password
	Password: A minimum of 6-character field to login the user after validation

Validation:
	The users first name, last name, email and password are validated as follows:
		Data provided in the 4 fields are taken and validated to check for existing customer.
			If user exists then the data is not stored in the database
			If the user doesn’t exist, the data is stored, and the user is registered

Register User Button:
	This button will validate the user data for website standards of characters and minimum length:
		Successful login: Redirect to Signup showing a user is created message
		Unsuccessful login: Redirect to Signup page with a failure to register message 

Index Page:
 
The purpose of the page is to introduce the webpage and what it is about, with a link to connections page to start off with using the website.

Audience:
People who have signed up to the website or willing to sign up to the website. The other audience include people who enter the website for the very first time and want to know how the website works or want to see all available connections.

There are 0 fields on this page:
	This page has no fields associated with it.

Validation:
	No optional validation is required with this page.

Register User Button:
	Connections Button: Used to redirect the user to either viewing the connections or creating one.

Connections Page:
 
The purpose of the page is to let the user view all available connections created by the user and other users. Typically, in this page a user can edit and delete their connections on the fly (meaning the card is unlocked). Here the user can also view other authors connections but cannot edit/delete the connection(meaning the card is locked)

Audience:
Users who wish to join someone’s event or add and see what their connection to events look like.

There are 0 fields on this page:
	This page has no fields associated with it.

Validation:
	No optional validation is required with this page.

Edit and Delete Button:
	Edit button is used to edit the connection right away directly form the connection page.
	Delete button is used to delete the connection right away directly form the connection page.

My Connections Page:
 
The purpose of the page is to let the user view all the connections that a user has RSVP'd into. This page holds the complete information about the event and an edit update button.

Audience:
The user who is logged in and wish to RSVP to an event.

There are 0 fields on this page:
	No fields present

Validation:
	No optional validation is required with this page.

Update and Delete Button:
	Update button is used to update RSVP right away directly form the page.
	Delete button is used to delete the RSVP and the connection associated right away directly from this page.

Start a new connection Page:
 
The purpose of the page is to let the user create a new connection that is then available for view to all other users.

Audience:
The user who is logged in and wish to create an event that others want to join.

There are 7 fields on this page:
	Connection Name
	Connection Category
	Host Full Name
	Planned date
	What Time
	Description about this connection
	Event Location

Validation:
	All the fields are validated for correct strings count and length before an event is created.

Create Connection Button:
	Create connection button is used to create a connection after successful validation.


About Page:
 
The purpose of the page is to let the user know what this website is all about.

Audience:
People interested in knowing the website

There are 0 fields on this page:
	NA

Validation:
	NA

Create Connection Button:
	NA

Contact Page:
 
The purpose of the page is to let the user know who created this website.

Audience:
People interested in knowing the author of the website.

There are 0 fields on this page:
	NA

Validation:
	NA

Create Connection Button:
	NA

 
 
